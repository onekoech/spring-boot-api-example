package co.ke.collengine.springbootapiexample;

import co.ke.collengine.springbootapiexample.entity.ThemeParkRide;
import co.ke.collengine.springbootapiexample.repository.ThemeParkRideRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class SpringBootApiExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootApiExampleApplication.class);
        String status = setName("Peter", "Kipkoech");
        // end of main
    }

    @Bean
    public CommandLineRunner sampleData(ThemeParkRideRepository repository) {
        return (args) -> {
            repository.save(new ThemeParkRide("Rollercoaster", "Train ride that speeds you along.", 5, 3));
            repository.save(new ThemeParkRide("Log flume", "Boat ride with plenty of splashes.", 3, 2));
            repository.save(new ThemeParkRide("Teacups", "Spinning ride in a giant tea-cup.", 2, 4));
        };

    }


    public static String setName(String firstName, String lastName){
        return "Name set successfully";
    }
}
